package ru.efko.task.errors;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.SQLException;

/**
 * Created by camel on 11.03.17.
 */
@ControllerAdvice
public class ExceptionsHandler {
    @ExceptionHandler(value = SQLException.class)
    @ResponseBody public String processSqlException(SQLException exception) {
        // TODO: return proper error codes
        return "Database Error: " + exception.getMessage();
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody public String processException(Exception exception) {
        // TODO: return proper error codes
        return "Error: " + exception.getMessage();
    }

}
