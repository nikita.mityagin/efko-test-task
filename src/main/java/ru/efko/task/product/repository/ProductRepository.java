package ru.efko.task.product.repository;

import org.springframework.data.repository.CrudRepository;
import ru.efko.task.product.domain.Product;

import javax.transaction.Transactional;

/**
 * Created by camel on 11.03.17.
 */
@Transactional
public interface ProductRepository extends CrudRepository<Product, Long> {
}
