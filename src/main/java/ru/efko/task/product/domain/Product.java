package ru.efko.task.product.domain;

import lombok.Data;
import ru.efko.task.product.model.ProductDto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by camel on 11.03.17.
 */
@Data
@Entity(name = "products")
public class Product {
    @Id
    @GeneratedValue
    Long id;

    @Column(name = "namee", nullable = false)
    String name;

    @Column(nullable = false)
    Long cost;

    public static Product fromDto(ProductDto productDto) {
        Product product = new Product();
        product.setName(productDto.getName());
        product.setCost(productDto.getCost());

        return product;
    }
}
