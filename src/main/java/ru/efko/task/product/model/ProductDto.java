package ru.efko.task.product.model;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;

/**
 * Created by camel on 11.03.17.
 */
@Data
public class ProductDto {
    @JsonView(ProductView.Public.class)
    private String name;

    @JsonView(ProductView.Public.class)
    private Long cost;

    public static class ProductView {
        public static class Public {}
    }
}
