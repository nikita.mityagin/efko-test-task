package ru.efko.task.product.web;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import ru.efko.task.product.domain.Product;
import ru.efko.task.product.model.ProductDto;
import ru.efko.task.product.service.ProductService;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by camel on 11.03.17.
 */
@Log4j
@Controller
@SuppressWarnings("unused")
public class ProductRestController {
    @Autowired
    private ProductService productService;

    @GetMapping("products")
    @JsonView(ProductDto.ProductView.Public.class)
    public Collection<Product> getProducts(
            @RequestHeader(value = "Authorization") String authorization) {
        log.debug("Start. Getting the products.");

        if (isAuthorized(authorization)) {
            Collection<Product> products = productService.getProducts();

            log.debug("End. Returning " + products.size() + " products.");

            return products;
        }

        // TODO: return FORBIDDEN and human readable message
        log.debug("End. Not authorised.");

        return Collections.emptyList();
    }

    @PostMapping("products")
    public void saveProduct(
            @RequestHeader(value = "Authorization") String authorization,
            @ModelAttribute ProductDto productDto) {
        log.debug("Start. Getting the products.");

        if (isAuthorized(authorization)) {
            Product product = productService.saveProduct(productDto);
            log.debug("End. Saved product: " + product);
        } else {
            // TODO: return FORBIDDEN and human readable message
            log.debug("End. Not authorised.");
        }

    }

    private boolean isAuthorized(String authorizationToken) {
        // Here can be a call to authorization service or to some logic
        // in this service.
        return true;
    }
}
