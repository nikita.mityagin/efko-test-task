package ru.efko.task.product.service;

import ru.efko.task.product.model.ProductDto;
import ru.efko.task.product.domain.Product;

import java.util.Collection;

/**
 * Created by camel on 11.03.17.
 */
public interface ProductService {

    Collection<Product> getProducts();

    Product saveProduct(ProductDto productDto);
}
