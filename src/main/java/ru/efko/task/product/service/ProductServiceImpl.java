package ru.efko.task.product.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.efko.task.product.model.ProductDto;
import ru.efko.task.product.domain.Product;
import ru.efko.task.product.repository.ProductRepository;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Created by camel on 11.03.17.
 */
@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    public Collection<Product> getProducts() {
        Collection<Product> products = new LinkedList<>();

        productRepository.findAll().forEach(product -> {products.add(product);});

        return products;
    }

    public Product saveProduct(ProductDto productDto) {
        return productRepository.save(Product.fromDto(productDto));
    }
}
