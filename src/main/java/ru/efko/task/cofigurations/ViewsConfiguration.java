package ru.efko.task.cofigurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;

import java.util.Collections;

/**
 * Created by camel on 11.03.17.
 */
@Configuration
@EnableWebMvc
@SuppressWarnings("unused")
public class ViewsConfiguration extends WebMvcConfigurerAdapter {

    @Bean
    public ViewResolver contentNegotiatingViewResolver(ContentNegotiationManager manager) {
        ContentNegotiatingViewResolver resolver = new ContentNegotiatingViewResolver();
        resolver.setViewResolvers(Collections.singletonList(new JsonViewResolver()));
        resolver.setContentNegotiationManager(manager);

        return resolver;
    }
}
