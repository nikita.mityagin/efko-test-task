Description:

There is simple Rest service with API:
- GET /products
- POST /products?name=Not Null String&cost=123 (parameters are required)

You need to specify **Authorization** header with any string as value 
(authentication & authorization are not implemented, there only stub call
which can be replaced with cal to authorization service, as you can see in
controller class)

Deployed on heroku:
https://agile-hollows-94593.herokuapp.com/
